-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 29-06-2020 a las 12:20:27
-- Versión del servidor: 10.3.23-MariaDB-1:10.3.23+maria~bionic-log
-- Versión de PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `edemocracia`
--
CREATE DATABASE IF NOT EXISTS `edemocracia` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `edemocracia`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_actoadministrativo`
--

CREATE TABLE `bacata_actoadministrativo` (
  `idacto` varchar(2) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(150) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_actoadministrativo`
--

TRUNCATE TABLE `bacata_actoadministrativo`;
--
-- Volcado de datos para la tabla `bacata_actoadministrativo`
--

INSERT INTO `bacata_actoadministrativo` (`idacto`, `descripcion`) VALUES
('RE', 'Resolucion'),
('OF', 'Oficio'),
('ME', 'Memorando'),
('LE', 'Ley'),
('DE', 'Decreto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_audit`
--

CREATE TABLE `bacata_audit` (
  `date` datetime NOT NULL,
  `user` varchar(30) DEFAULT NULL,
  `host` varchar(40) DEFAULT NULL,
  `object_type` varchar(20) DEFAULT NULL,
  `object_name` varchar(30) DEFAULT NULL,
  `column_name` varchar(50) DEFAULT NULL,
  `v_primarykey` text DEFAULT NULL,
  `new_value` text DEFAULT NULL,
  `old_value` text DEFAULT NULL,
  `operation` char(1) DEFAULT NULL,
  `row` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla de auditoria para la BD plataforma_voto';

--
-- Truncar tablas antes de insertar `bacata_audit`
--

TRUNCATE TABLE `bacata_audit`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_bitacora`
--

CREATE TABLE `bacata_bitacora` (
  `id_nota` int(15) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  `usuario` int(15) NOT NULL,
  `nota` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_bitacora`
--

TRUNCATE TABLE `bacata_bitacora`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_bloque`
--

CREATE TABLE `bacata_bloque` (
  `id_bloque` int(5) NOT NULL,
  `nombre` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupo` char(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bloques disponibles' PACK_KEYS=0;

--
-- Truncar tablas antes de insertar `bacata_bloque`
--

TRUNCATE TABLE `bacata_bloque`;
--
-- Volcado de datos para la tabla `bacata_bloque`
--

INSERT INTO `bacata_bloque` (`id_bloque`, `nombre`, `descripcion`, `grupo`) VALUES
(1, 'banner', 'Banner principal del aplicativo.', 'gui'),
(2, 'login', 'Bloque para autenticar a los usuarios del portal.', 'registro'),
(3, 'slidePrincipal', 'Bloque que contiene el slide de la página principal del aplicativo', 'gui'),
(4, 'pie', 'Pie de página genérico para el aplicativo.', 'gui'),
(5, 'menuAdministradorHorizontal', 'Menu del usuario administrador', 'administrador'),
(6, 'slider', 'Slider de imagenes para el inicio de pagina', 'gui'),
(7, 'procesoElectoral', 'Bloque que permite crear nuevos procesos electorales', 'administrador'),
(8, 'bannerVotante', 'Baner para el módulo de votantes.', 'gui'),
(9, 'menuVotante', 'Menu lateral del módulo de votante', 'votante'),
(10, 'adminEleccionesVotante', 'Listado enriquecido que contiene las elecciones presentes y futuras en las que está registrado un documento de identidad.', 'votante/admin'),
(11, 'parametrizarProcesoElectoral', 'Bloque que permite parametrizar procesos electorales previamente creados', 'administrador'),
(12, 'subirCenso', 'Bloque que permite subir el censo', 'administrador'),
(13, 'votoTarjeton', 'Bloque que permite ver las votaciones activas del usuario y permite realizar el voto', 'votante'),
(14, 'menuDelegadoHorizontal', 'Menú lateral para el prefil de delegado.', 'delegado/menu'),
(15, 'registroInicioProceso', 'Bloque para las actividades de inicio de proceso de votación.', 'delegado/registro'),
(16, 'registroCierreProceso', 'Bloque con las actividades de cierre de proceso de participación', 'delegado/registro'),
(17, 'resultados', 'Bloque para el cálculo de los resultados por parte del delegado', 'delegado/registro'),
(18, 'cerrarSesion', 'Bloque para cerrar la sesion a los usuarios del portal', 'registro'),
(19, 'menuVeedor', 'Menu de administracion del veedor de las votaciones', 'veedor'),
(20, 'bitacora', 'Bloque que permite crear la bitacora de votacion', 'veedor'),
(21, 'resultados', 'Bloque que permite revisar la votacion', 'veedor'),
(22, 'consultaProceso', 'Bloque que permite verificar el proceso total de votacion', 'veedor'),
(23, 'procesoElectoral', 'Bloque que permite la gestion electoral', 'veedor'),
(24, 'parametrizarProcesoElectoral', 'Bloque que permite la gestion electoral', 'veedor'),
(25, 'cambiarClave', 'Bloque para cambiar la clave a los usuarios del portal', 'registro'),
(26, 'segundaClave', 'Registro de la segunda clave del votante', 'votante'),
(27, 'votaciones', 'Bloque que permite verificar el proceso de votaciones', 'veedor/proceso'),
(28, 'certificados', 'Bloque que permite verificar el proceso de generacion de certificados', 'veedor/proceso'),
(29, 'accesos', 'Bloque que permite verificar el proceso de accesos a la plataforma', 'veedor/proceso'),
(30, 'accesosFallidos', 'Bloque que permite verificar el proceso de accesos fallidos a la plataforma', 'veedor/proceso'),
(31, 'hashCodigoFuente', 'Bloque que permite realizar la validación del hash del codigo fuente', 'administrador'),
(32, 'mensajeLateral', 'Mensaje de ayuda que se muestra en la sección lateral de las páginas', 'gui'),
(33, 'gestionUsuarios', 'Pagina de Administracion de usuarios de la plataforma', 'administrador'),
(34, 'cambiarPrimeraClave', 'Bloque para cambiar la primera clave a los usuarios del portal', 'registro'),
(35, 'menuJurado', 'Menu de jurado de salas de votaciones', 'jurado'),
(36, 'bitacoraJurado', 'Bloque que permite crear la bitacora de votacion de acceso a salas', 'jurado'),
(37, 'consultaCenso', 'Pagina que permite consultar el censo al jurado de acceso a salas', 'jurado'),
(38, 'menuJurado', 'Menu de jurado de salas de votaciones', 'jurado'),
(39, 'bitacoraJurado', 'Bloque que permite crear la bitacora de votacion de acceso a salas', 'jurado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_bloque_pagina`
--

CREATE TABLE `bacata_bloque_pagina` (
  `id_pagina` int(5) NOT NULL DEFAULT 0,
  `id_bloque` int(5) NOT NULL DEFAULT 0,
  `seccion` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `posicion` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Estructura de bloques de las paginas en el aplicativo';

--
-- Truncar tablas antes de insertar `bacata_bloque_pagina`
--

TRUNCATE TABLE `bacata_bloque_pagina`;
--
-- Volcado de datos para la tabla `bacata_bloque_pagina`
--

INSERT INTO `bacata_bloque_pagina` (`id_pagina`, `id_bloque`, `seccion`, `posicion`) VALUES
(1, 2, 'D', 1),
(1, 3, 'B', 1),
(1, 4, 'E', 1),
(2, 1, 'A', 1),
(2, 4, 'E', 1),
(2, 5, 'A', 2),
(2, 6, 'C', 1),
(3, 1, 'A', 1),
(3, 4, 'E', 1),
(3, 5, 'A', 2),
(3, 7, 'C', 1),
(4, 4, 'E', 1),
(4, 8, 'A', 1),
(4, 9, 'B', 1),
(4, 13, 'C', 1),
(5, 1, 'A', 1),
(5, 4, 'E', 1),
(5, 5, 'A', 2),
(5, 11, 'C', 1),
(6, 1, 'A', 1),
(6, 4, 'E', 1),
(6, 5, 'A', 2),
(6, 12, 'C', 1),
(7, 4, 'E', 1),
(7, 8, 'A', 1),
(7, 9, 'B', 1),
(7, 13, 'C', 1),
(7, 32, 'B', 2),
(8, 1, 'A', 1),
(8, 3, 'C', 1),
(8, 4, 'E', 1),
(8, 14, 'A', 2),
(9, 1, 'A', 1),
(9, 4, 'E', 1),
(9, 14, 'A', 2),
(9, 15, 'C', 1),
(10, 1, 'A', 1),
(10, 4, 'E', 1),
(10, 14, 'A', 2),
(10, 16, 'C', 1),
(11, 1, 'A', 1),
(11, 4, 'E', 1),
(11, 14, 'A', 2),
(11, 22, 'C', 1),
(12, 1, 'A', 1),
(12, 4, 'E', 1),
(12, 14, 'A', 2),
(12, 25, 'C', 1),
(13, 18, 'C', 1),
(14, 1, 'A', 1),
(14, 4, 'E', 1),
(14, 19, 'B', 1),
(15, 1, 'A', 1),
(15, 4, 'E', 1),
(15, 19, 'B', 1),
(15, 27, 'C', 1),
(16, 1, 'A', 1),
(16, 4, 'E', 1),
(16, 19, 'B', 1),
(16, 28, 'C', 1),
(17, 1, 'A', 1),
(17, 4, 'E', 1),
(17, 19, 'B', 1),
(17, 29, 'C', 1),
(18, 1, 'A', 1),
(18, 4, 'E', 1),
(18, 19, 'B', 1),
(18, 30, 'C', 1),
(19, 1, 'A', 1),
(19, 4, 'E', 1),
(19, 19, 'B', 1),
(19, 20, 'C', 1),
(20, 1, 'A', 1),
(20, 4, 'E', 1),
(20, 19, 'B', 1),
(20, 21, 'C', 1),
(21, 1, 'A', 1),
(21, 4, 'E', 1),
(21, 19, 'B', 1),
(21, 22, 'C', 1),
(22, 1, 'A', 1),
(22, 4, 'E', 1),
(22, 19, 'B', 1),
(22, 25, 'C', 1),
(23, 1, 'A', 1),
(23, 4, 'E', 1),
(23, 5, 'A', 2),
(23, 25, 'C', 1),
(24, 18, 'C', 1),
(25, 18, 'C', 1),
(26, 4, 'E', 1),
(26, 8, 'A', 1),
(26, 9, 'B', 1),
(26, 25, 'C', 1),
(27, 4, 'E', 1),
(27, 8, 'A', 1),
(27, 9, 'B', 1),
(27, 26, 'C', 1),
(28, 18, 'C', 1),
(29, 1, 'A', 1),
(29, 4, 'E', 1),
(29, 5, 'A', 2),
(29, 31, 'C', 1),
(30, 1, 'A', 1),
(30, 4, 'E', 1),
(30, 5, 'A', 2),
(30, 33, 'C', 1),
(31, 25, 'C', 1),
(32, 1, 'A', 1),
(32, 4, 'E', 1),
(32, 35, 'B', 1),
(33, 1, 'A', 1),
(33, 4, 'E', 1),
(33, 35, 'B', 1),
(33, 36, 'C', 1),
(34, 1, 'A', 1),
(34, 4, 'E', 1),
(34, 35, 'B', 1),
(34, 37, 'C', 1),
(35, 1, 'A', 1),
(35, 4, 'E', 1),
(35, 25, 'C', 1),
(35, 35, 'B', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_candidato`
--

CREATE TABLE `bacata_candidato` (
  `idcandidato` int(11) NOT NULL,
  `identificacion` int(11) DEFAULT NULL,
  `nombre` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reglon` int(11) DEFAULT NULL,
  `lista_idlista` int(11) NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_candidato`
--

TRUNCATE TABLE `bacata_candidato`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_censo`
--

CREATE TABLE `bacata_censo` (
  `identificacion` bigint(25) NOT NULL,
  `clave` char(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'sha1(md5())',
  `ideleccion` int(11) NOT NULL,
  `nombre` char(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nombre Completo del Elector',
  `idtipo` int(11) NOT NULL COMMENT 'Tipo de estamento al que pertenece el elector',
  `segunda_identificacion` bigint(15) NOT NULL,
  `fechavoto` datetime DEFAULT NULL,
  `datovoto` varchar(528) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Datos de identificación electrónica: IP',
  `acep_termi` int(15) DEFAULT NULL,
  `acep_termi_tele` int(15) DEFAULT NULL,
  `acep_termi_celu` int(15) DEFAULT NULL,
  `acep_termi_direccion` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expira_clave` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `correo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Censo y datos de la votación';

--
-- Truncar tablas antes de insertar `bacata_censo`
--

TRUNCATE TABLE `bacata_censo`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_configuracion`
--

CREATE TABLE `bacata_configuracion` (
  `id_parametro` int(3) NOT NULL,
  `parametro` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` char(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Variables de configuracion';

--
-- Truncar tablas antes de insertar `bacata_configuracion`
--

TRUNCATE TABLE `bacata_configuracion`;
--
-- Volcado de datos para la tabla `bacata_configuracion`
--

INSERT INTO `bacata_configuracion` (`id_parametro`, `parametro`, `valor`) VALUES
(1, 'prefijo', 'bacata_'),
(2, 'nombreAplicativo', 'Bacatá | Plataforma E-democracia'),
(3, 'raizDocumento', '/var/www/html/bacata'),
(4, 'host', 'http://localhost'),
(5, 'site', '/bacata'),
(6, 'nombreAdministrador', 'administrador'),
(7, 'claveAdministrador', 'kgCOz_N7GlJAAjFLpuM'),
(8, 'correoAdministrador', 'ttg@ttg.com.co'),
(9, 'enlace', 'edemocracia'),
(10, 'googlemaps', ''),
(11, 'recatchapublica', ' 	6LdUuPISAAAAAL_oC7eHaRldEoykUBB0eogw8gAE'),
(12, 'recatchaprivada', ' 	6LdUuPISAAAAAK3F4oYTop925AhgioIRX-mEkCVO'),
(13, 'expiracion', '5'),
(14, 'instalado', 'true'),
(15, 'debugMode', 'false'),
(16, 'dbPrincipal', 'edemocracia'),
(17, 'hostSeguro', 'https://localhost/'),
(18, 'public_key', 'file:///var/soportes/edemocracia/llaves/'),
(19, 'private_key', 'file:///var//soportes/edemocracia/llaves/'),
(22, 'host_amazon', 'https://10.20.0.245'),
(23, 'rutaCandidatos', '/var/soportes/edemocracia/candidatos/'),
(24, 'raizDocumentoTemp', '/var/soportes/edemocracia'),
(25, 'urlCandidatos', '/edemocracia/candidatos/'),
(26, 'rutaLlaves', '/var/soportes/edemocracia/llaves/'),
(27, 'expiracion_admin', '30'),
(28, 'max_fecha_reclamacion', '03/07/2017-05/11/2017'),
(29, 'tiempo_vida_clave', '20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_datovoto`
--

CREATE TABLE `bacata_datovoto` (
  `idusuario` bigint(11) NOT NULL,
  `ideleccion` int(11) NOT NULL,
  `fecha` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Se registra una vez el usuario marque su voto';

--
-- Truncar tablas antes de insertar `bacata_datovoto`
--

TRUNCATE TABLE `bacata_datovoto`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_dbms`
--

CREATE TABLE `bacata_dbms` (
  `id` int(11) NOT NULL,
  `nombre` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `dbms` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `servidor` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `puerto` int(6) NOT NULL,
  `conexionssh` char(50) COLLATE utf8_unicode_ci NOT NULL,
  `db` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_dbms`
--

TRUNCATE TABLE `bacata_dbms`;
--
-- Volcado de datos para la tabla `bacata_dbms`
--

INSERT INTO `bacata_dbms` (`id`, `nombre`, `dbms`, `servidor`, `puerto`, `conexionssh`, `db`, `usuario`, `password`) VALUES
(1, 'estructura', 'mysql', 'localhost', 3306, '', 'edemocracia', 'admin_voto', 'YwDKjCVJoVxGmMVTUnOfnJt75A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_dependencias`
--

CREATE TABLE `bacata_dependencias` (
  `id_dependencia` int(5) NOT NULL,
  `nombre` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_dependencias`
--

TRUNCATE TABLE `bacata_dependencias`;
--
-- Volcado de datos para la tabla `bacata_dependencias`
--

INSERT INTO `bacata_dependencias` (`id_dependencia`, `nombre`) VALUES
(1, 'ADMINISTRACION'),
(2, 'SECRETARIA GENERAL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_eleccion`
--

CREATE TABLE `bacata_eleccion` (
  `ideleccion` int(11) NOT NULL,
  `procesoelectoral_idprocesoelectoral` int(11) NOT NULL,
  `nombre` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipoestamento` int(11) DEFAULT NULL,
  `descripcion` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechainicio` timestamp NULL DEFAULT NULL,
  `fechafin` timestamp NULL DEFAULT NULL,
  `listaTarjeton` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipovotacion` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `candidatostarjeton` int(11) DEFAULT NULL,
  `utilizarsegundaclave` int(11) DEFAULT NULL,
  `eleccionform` int(11) NOT NULL,
  `tiporesultado` int(2) NOT NULL DEFAULT 1,
  `porcEstudiante` float NOT NULL DEFAULT 0,
  `porcDocente` float NOT NULL DEFAULT 0,
  `porcEgresado` float NOT NULL DEFAULT 0,
  `porcFuncionario` float NOT NULL DEFAULT 0,
  `porcDocenteVinEspecial` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_eleccion`
--

TRUNCATE TABLE `bacata_eleccion`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_estilo`
--

CREATE TABLE `bacata_estilo` (
  `usuario` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `estilo` char(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Estilo de pagina en el sitio';

--
-- Truncar tablas antes de insertar `bacata_estilo`
--

TRUNCATE TABLE `bacata_estilo`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_lista`
--

CREATE TABLE `bacata_lista` (
  `idlista` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eleccion_ideleccion` int(11) NOT NULL,
  `posiciontarjeton` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_lista`
--

TRUNCATE TABLE `bacata_lista`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_llave_seguridad`
--

CREATE TABLE `bacata_llave_seguridad` (
  `idllave` int(11) NOT NULL,
  `idproceso` int(11) NOT NULL,
  `tipollave` int(11) NOT NULL,
  `nombrellave` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_llave_seguridad`
--

TRUNCATE TABLE `bacata_llave_seguridad`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_logger`
--

CREATE TABLE `bacata_logger` (
  `id` int(10) NOT NULL,
  `evento` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` char(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Registro de acceso de los usuarios';

--
-- Truncar tablas antes de insertar `bacata_logger`
--

TRUNCATE TABLE `bacata_logger`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_nodos`
--

CREATE TABLE `bacata_nodos` (
  `id_nodo` int(10) NOT NULL,
  `nombre_nodo` varchar(100) CHARACTER SET ucs2 COLLATE ucs2_unicode_ci NOT NULL,
  `ip_nodo` varchar(255) CHARACTER SET ucs2 COLLATE ucs2_unicode_ci NOT NULL,
  `ruta_nodo` varchar(255) CHARACTER SET ucs2 COLLATE ucs2_unicode_ci NOT NULL,
  `user` varchar(255) CHARACTER SET ucs2 COLLATE ucs2_unicode_ci NOT NULL,
  `pass` varchar(255) CHARACTER SET ucs2 COLLATE ucs2_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_nodos`
--

TRUNCATE TABLE `bacata_nodos`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_pagina`
--

CREATE TABLE `bacata_pagina` (
  `id_pagina` int(5) NOT NULL,
  `nombre` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `descripcion` char(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `modulo` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nivel` int(2) NOT NULL DEFAULT 0,
  `parametro` char(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_pagina`
--

TRUNCATE TABLE `bacata_pagina`;
--
-- Volcado de datos para la tabla `bacata_pagina`
--

INSERT INTO `bacata_pagina` (`id_pagina`, `nombre`, `descripcion`, `modulo`, `nivel`, `parametro`) VALUES
(1, 'index', 'Página principal de la Plataforma de votación electrónica.', 'General', 0, 'jquery=true&jquery-ui=true'),
(2, 'indexAdministrador', 'Pagina principal del administrador del sistema', 'Administrador', 4, 'jquery=true'),
(3, 'procesoElectoral', 'Modulo que permite crear un proceso electoral desde cero', 'Administrador', 4, 'jquery=true'),
(4, 'indexVotante', 'Página principal para el votante', 'votante', 1, 'jquery=true&jquery-ui=true'),
(5, 'parametrizarProcesoElectoral', 'Pagina que permite realizar la parametrización de un proceso seleccionado', 'Administrador', 4, 'jquery=true'),
(6, 'subirCenso', 'Modulo que permite subir el censo', 'Administrador', 4, 'jquery=true'),
(7, 'votacionesVotante', 'Pagina que permite listar las votaciones activas del votante', 'votante', 1, 'jquery=true&jquery-ui=true'),
(8, 'indexDelegado', 'Página de entrada al módulo de Delegado Electoral', 'delegado', 3, 'jquery=true'),
(9, 'iniciarProceso', 'Página desde donde se realizan las actividades de inicio del proceso de votaciones.', 'delegado', 3, 'jquery=true'),
(10, 'cerrarProceso', 'Bloque para las actividades de cierre de proceso de votación.', 'delegado', 3, 'jquery=true'),
(11, 'seguimientoProceso', 'Página para el seguimiento del proceso por parte del delegado', 'delegado', 3, 'jquery=true'),
(12, 'cambiarClaveDelegado', 'Modificación de la clave', 'delegado', 3, 'jquery=true'),
(13, 'cerrarSesionDelegado', 'Pagina que permite cerrar la sesion del usuario delegado', 'delegado', 3, 'jquery=true'),
(14, 'indexVeedor', 'Pagina inicial del veedor de la votaciones', 'veedor', 2, 'jquery=true'),
(15, 'procesoVotaciones', 'Pagina que permite verificar el proceso de votaciones', 'veedor', 2, 'jquery=true'),
(16, 'procesoCertificados', 'Pagina que permite verificar el proceso de generacion de certificados', 'veedor', 2, 'jquery=true'),
(17, 'procesoAccesos', 'Pagina que permite verificar el proceso de accesos a la plataforma', 'veedor', 2, 'jquery=true'),
(18, 'procesoAccesosFallidos', 'Pagina que permite verificar el proceso de accesos fallidos a la plataforma', 'veedor', 2, 'jquery=true'),
(19, 'bitacoraVeedor', 'Pagina que permite registrar la bitacora de votacion', 'veedor', 2, 'jquery=true'),
(20, 'resultadosVeedor', 'Pagina que permite revisar los resultados de la votacion', 'veedor', 2, 'jquery=true'),
(21, 'consultaProceso', 'Pagina que permite verificar el proceso total de la votacion', 'veedor', 2, 'jquery=true'),
(22, 'cambiarClaveVeedor', 'Modificación de la clave', 'veedor', 2, 'jquery=true'),
(23, 'cambiarClaveAdministrador', 'Modificación de la clave', 'Administrador', 4, 'jquery=true'),
(24, 'cerrarSesionAdministrador', 'Pagina que permite cerrar la sesion del usuario administrador', 'Administrador', 4, 'jquery=true'),
(25, 'cerrarSesionVotante', 'Pagina que permite cerrar la sesion del usuario votante', 'votante', 1, 'jquery=true'),
(26, 'cambiarClaveVotante', 'Modificación de la clave', 'votante', 1, 'jquery=true'),
(27, 'segundaClaveVotante', 'Registro de la segunda clave del votante', 'votante', 1, 'jquery=true'),
(28, 'cerrarSesionVeedor', 'Cerrar la sesion del usuario veedor', 'veedor', 2, 'jquery=true'),
(29, 'hashCodigoFuente', 'Pagina que permite revisar el hash del codigo fuente', 'Administrador', 4, 'jquery=true'),
(30, 'gestionUsuarios', 'Pagina de Administracion de usuarios de la plataforma', 'Administrador', 4, 'jquery=true'),
(31, 'cambioPrimeraClave', 'Modulo que permite cambiar por primera vez la clave de acceso', 'general', 0, 'jquery=true'),
(32, 'indexSalas', 'Pagina principal Control de salas', 'jurado', 5, 'jquery=true'),
(33, 'bitacoraJurado', 'Pagina que permite registrar la bitacora de votacion en Control de salas', 'jurado', 5, 'jquery=true'),
(34, 'consultarCenso', 'Pagina que permite consultar el censo al jurado en Control de salas', 'jurado', 5, 'jquery=true'),
(35, 'cambiarClaveJurado', 'Pagina que permite cambio clave', 'jurado', 5, 'jquery=true'),
(36, 'cerrarSesionJurado', 'Pagina que permite cerrar sesion', 'jurado', 5, 'jquery=true'),
(37, 'indexSalas', 'Pagina principal Control de salas', 'jurado', 5, 'jquery=true'),
(38, 'bitacoraJurado', 'Pagina que permite registrar la bitacora de votacion en Control de salas', 'jurado', 5, 'jquery=true');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_preguntasegundaclave`
--

CREATE TABLE `bacata_preguntasegundaclave` (
  `idpregunta` int(10) NOT NULL,
  `descripcion` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_preguntasegundaclave`
--

TRUNCATE TABLE `bacata_preguntasegundaclave`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_procesoelectoral`
--

CREATE TABLE `bacata_procesoelectoral` (
  `idprocesoelectoral` int(11) NOT NULL,
  `nombre` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechainicio` timestamp NULL DEFAULT NULL,
  `fechafin` timestamp NULL DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `cantidadelecciones` int(11) DEFAULT NULL,
  `dependenciasresponsables` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipoactoadministrativo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idactoadministrativo` int(11) DEFAULT NULL,
  `fechaactoadministrativo` date DEFAULT NULL,
  `tipovotacion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_procesoelectoral`
--

TRUNCATE TABLE `bacata_procesoelectoral`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_segundaclave`
--

CREATE TABLE `bacata_segundaclave` (
  `identificacion` int(15) NOT NULL,
  `idpregunta` int(5) NOT NULL,
  `respuesta` varchar(255) CHARACTER SET ucs2 COLLATE ucs2_unicode_ci NOT NULL,
  `segundaclave` varchar(200) CHARACTER SET ucs2 COLLATE ucs2_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_segundaclave`
--

TRUNCATE TABLE `bacata_segundaclave`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_subsistema`
--

CREATE TABLE `bacata_subsistema` (
  `id_subsistema` int(7) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `etiqueta` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_pagina` int(7) NOT NULL DEFAULT 0,
  `observacion` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Subsistemas que componen el aplicativo' PACK_KEYS=0;

--
-- Truncar tablas antes de insertar `bacata_subsistema`
--

TRUNCATE TABLE `bacata_subsistema`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_tempFormulario`
--

CREATE TABLE `bacata_tempFormulario` (
  `id_sesion` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `formulario` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `campo` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `valor` text COLLATE utf8_unicode_ci NOT NULL,
  `fecha` char(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_tempFormulario`
--

TRUNCATE TABLE `bacata_tempFormulario`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_tipoestamento`
--

CREATE TABLE `bacata_tipoestamento` (
  `idtipo` int(5) NOT NULL,
  `descripcion` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `ponderado` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_tipoestamento`
--

TRUNCATE TABLE `bacata_tipoestamento`;
--
-- Volcado de datos para la tabla `bacata_tipoestamento`
--

INSERT INTO `bacata_tipoestamento` (`idtipo`, `descripcion`, `ponderado`) VALUES
(1, 'Egresados', 0),
(2, 'Administrativos', 0),
(3, 'Docentes', 0),
(4, 'Estudiantes', 0),
(5, 'Contratistas', 0),
(6, 'Docentes vinculación especial', 0),
(7, 'Todos', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_tiporestriccion`
--

CREATE TABLE `bacata_tiporestriccion` (
  `idtipo` int(5) NOT NULL,
  `descripcion` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_campo` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_tiporestriccion`
--

TRUNCATE TABLE `bacata_tiporestriccion`;
--
-- Volcado de datos para la tabla `bacata_tiporestriccion`
--

INSERT INTO `bacata_tiporestriccion` (`idtipo`, `descripcion`, `nombre_campo`) VALUES
(1, 'Todos', 'todos'),
(2, 'Excluir Egresados', 'egresados'),
(3, 'Excluir Funcionarios', 'funcionarios'),
(4, 'Exluir Contratistas', 'contratistas'),
(5, 'Excluir Estudiantes', 'estudiantes'),
(6, 'Excluir Docentes', 'docentes'),
(7, 'Excluir Docentes de vinculación especial', 'docentesve');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_tiporesultados`
--

CREATE TABLE `bacata_tiporesultados` (
  `idtipo` int(5) NOT NULL,
  `descripcion` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_tiporesultados`
--

TRUNCATE TABLE `bacata_tiporesultados`;
--
-- Volcado de datos para la tabla `bacata_tiporesultados`
--

INSERT INTO `bacata_tiporesultados` (`idtipo`, `descripcion`) VALUES
(1, 'Cálculo Normal'),
(2, 'Cálculo Ponderado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_tipousuario`
--

CREATE TABLE `bacata_tipousuario` (
  `idtipo` int(5) NOT NULL,
  `descripcion` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_tipousuario`
--

TRUNCATE TABLE `bacata_tipousuario`;
--
-- Volcado de datos para la tabla `bacata_tipousuario`
--

INSERT INTO `bacata_tipousuario` (`idtipo`, `descripcion`) VALUES
(1, 'Votante'),
(2, 'Veedor'),
(3, 'Jurado - Delegado'),
(4, 'Administrador'),
(5, 'Jurado - Sala');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_tipovotacion`
--

CREATE TABLE `bacata_tipovotacion` (
  `idtipo` int(5) NOT NULL,
  `descripcion` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_tipovotacion`
--

TRUNCATE TABLE `bacata_tipovotacion`;
--
-- Volcado de datos para la tabla `bacata_tipovotacion`
--

INSERT INTO `bacata_tipovotacion` (`idtipo`, `descripcion`) VALUES
(1, 'Presencial'),
(2, 'Internet'),
(3, 'Mixto'),
(4, 'Todos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_usuario`
--

CREATE TABLE `bacata_usuario` (
  `id_usuario` bigint(20) NOT NULL DEFAULT 0,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `apellido` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `correo` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `telefono` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `imagen` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clave` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estilo` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'basico',
  `idioma` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'es_es',
  `estado` int(1) NOT NULL DEFAULT 0 COMMENT '0=No activo, 1=Activo, 2=Por Cambio de Clave',
  `acep_termi` int(15) NOT NULL DEFAULT 0,
  `acep_termi_tele` int(15) NOT NULL DEFAULT 0,
  `acep_termi_celu` int(15) NOT NULL DEFAULT 0,
  `acep_termi_direccion` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `envioCorreo` int(1) NOT NULL DEFAULT 0,
  `imagenBlob` longblob NOT NULL DEFAULT '0',
  `pass_url` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncar tablas antes de insertar `bacata_usuario`
--

TRUNCATE TABLE `bacata_usuario`;
--
-- Volcado de datos para la tabla `bacata_usuario`
--

INSERT INTO `bacata_usuario` (`id_usuario`, `nombre`, `apellido`, `correo`, `telefono`, `imagen`, `clave`, `tipo`, `estilo`, `idioma`, `estado`, `acep_termi`, `acep_termi_tele`, `acep_termi_celu`, `acep_termi_direccion`, `envioCorreo`, `imagenBlob`, `pass_url`) VALUES
(9000390376, 'Admin', 'TTG Colombia', 'ttg@ttg.com.co', '3238924', '', 'b8477aceae74dc29fbe3b8541f52e606362069c5', '4', 'basico', 'es_es', 1, 0, 0, 0, '', 1, '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_usuario_subsistema`
--

CREATE TABLE `bacata_usuario_subsistema` (
  `id_usuario` int(6) NOT NULL DEFAULT 0,
  `id_subsistema` int(6) NOT NULL DEFAULT 0,
  `estado` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Relacion de usuarios que tienen acceso a modulos especiales';

--
-- Truncar tablas antes de insertar `bacata_usuario_subsistema`
--

TRUNCATE TABLE `bacata_usuario_subsistema`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_valor_sesion`
--

CREATE TABLE `bacata_valor_sesion` (
  `sesionId` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `variable` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `valor` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiracion` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Valores de sesion';

--
-- Truncar tablas antes de insertar `bacata_valor_sesion`
--

TRUNCATE TABLE `bacata_valor_sesion`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_votocodificado`
--

CREATE TABLE `bacata_votocodificado` (
  `idvoto` int(11) NOT NULL,
  `ideleccion` int(11) NOT NULL,
  `voto` text COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `estamento` int(5) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Registro de Votos';

--
-- Truncar tablas antes de insertar `bacata_votocodificado`
--

TRUNCATE TABLE `bacata_votocodificado`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bacata_votodecodificado`
--

CREATE TABLE `bacata_votodecodificado` (
  `idvoto` int(11) NOT NULL,
  `ideleccion` int(11) NOT NULL,
  `idlista` int(11) NOT NULL,
  `ip` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `estamento` int(5) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Se llena al final de la jornada  (votos decodificados)';

--
-- Truncar tablas antes de insertar `bacata_votodecodificado`
--

TRUNCATE TABLE `bacata_votodecodificado`;
--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bacata_bitacora`
--
ALTER TABLE `bacata_bitacora`
  ADD PRIMARY KEY (`id_nota`);

--
-- Indices de la tabla `bacata_bloque`
--
ALTER TABLE `bacata_bloque`
  ADD PRIMARY KEY (`id_bloque`),
  ADD KEY `id_bloque` (`id_bloque`);

--
-- Indices de la tabla `bacata_bloque_pagina`
--
ALTER TABLE `bacata_bloque_pagina`
  ADD PRIMARY KEY (`id_pagina`,`id_bloque`,`seccion`,`posicion`);

--
-- Indices de la tabla `bacata_candidato`
--
ALTER TABLE `bacata_candidato`
  ADD PRIMARY KEY (`idcandidato`),
  ADD KEY `fk_candidato_lista1_idx` (`lista_idlista`);

--
-- Indices de la tabla `bacata_censo`
--
ALTER TABLE `bacata_censo`
  ADD PRIMARY KEY (`identificacion`,`ideleccion`);

--
-- Indices de la tabla `bacata_configuracion`
--
ALTER TABLE `bacata_configuracion`
  ADD PRIMARY KEY (`id_parametro`),
  ADD KEY `parametro` (`parametro`);

--
-- Indices de la tabla `bacata_datovoto`
--
ALTER TABLE `bacata_datovoto`
  ADD PRIMARY KEY (`idusuario`,`ideleccion`),
  ADD UNIQUE KEY `idusuario` (`idusuario`,`ideleccion`);

--
-- Indices de la tabla `bacata_dbms`
--
ALTER TABLE `bacata_dbms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bacata_dependencias`
--
ALTER TABLE `bacata_dependencias`
  ADD PRIMARY KEY (`id_dependencia`),
  ADD UNIQUE KEY `id_dependencia` (`id_dependencia`);

--
-- Indices de la tabla `bacata_eleccion`
--
ALTER TABLE `bacata_eleccion`
  ADD PRIMARY KEY (`ideleccion`),
  ADD KEY `fk_eleccion_procesoelectoral_idx` (`procesoelectoral_idprocesoelectoral`);

--
-- Indices de la tabla `bacata_estilo`
--
ALTER TABLE `bacata_estilo`
  ADD PRIMARY KEY (`usuario`,`estilo`);

--
-- Indices de la tabla `bacata_lista`
--
ALTER TABLE `bacata_lista`
  ADD PRIMARY KEY (`idlista`),
  ADD KEY `fk_lista_eleccion1_idx` (`eleccion_ideleccion`);

--
-- Indices de la tabla `bacata_llave_seguridad`
--
ALTER TABLE `bacata_llave_seguridad`
  ADD PRIMARY KEY (`idllave`);

--
-- Indices de la tabla `bacata_logger`
--
ALTER TABLE `bacata_logger`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bacata_nodos`
--
ALTER TABLE `bacata_nodos`
  ADD PRIMARY KEY (`id_nodo`);

--
-- Indices de la tabla `bacata_pagina`
--
ALTER TABLE `bacata_pagina`
  ADD PRIMARY KEY (`id_pagina`),
  ADD UNIQUE KEY `id_pagina` (`id_pagina`);

--
-- Indices de la tabla `bacata_preguntasegundaclave`
--
ALTER TABLE `bacata_preguntasegundaclave`
  ADD PRIMARY KEY (`idpregunta`);

--
-- Indices de la tabla `bacata_procesoelectoral`
--
ALTER TABLE `bacata_procesoelectoral`
  ADD PRIMARY KEY (`idprocesoelectoral`);

--
-- Indices de la tabla `bacata_segundaclave`
--
ALTER TABLE `bacata_segundaclave`
  ADD PRIMARY KEY (`identificacion`);

--
-- Indices de la tabla `bacata_subsistema`
--
ALTER TABLE `bacata_subsistema`
  ADD PRIMARY KEY (`id_subsistema`);

--
-- Indices de la tabla `bacata_tempFormulario`
--
ALTER TABLE `bacata_tempFormulario`
  ADD KEY `id_sesion` (`id_sesion`);

--
-- Indices de la tabla `bacata_tipoestamento`
--
ALTER TABLE `bacata_tipoestamento`
  ADD PRIMARY KEY (`idtipo`);

--
-- Indices de la tabla `bacata_tiporestriccion`
--
ALTER TABLE `bacata_tiporestriccion`
  ADD PRIMARY KEY (`idtipo`);

--
-- Indices de la tabla `bacata_tiporesultados`
--
ALTER TABLE `bacata_tiporesultados`
  ADD PRIMARY KEY (`idtipo`);

--
-- Indices de la tabla `bacata_tipousuario`
--
ALTER TABLE `bacata_tipousuario`
  ADD PRIMARY KEY (`idtipo`);

--
-- Indices de la tabla `bacata_tipovotacion`
--
ALTER TABLE `bacata_tipovotacion`
  ADD PRIMARY KEY (`idtipo`);

--
-- Indices de la tabla `bacata_usuario`
--
ALTER TABLE `bacata_usuario`
  ADD UNIQUE KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `bacata_valor_sesion`
--
ALTER TABLE `bacata_valor_sesion`
  ADD PRIMARY KEY (`sesionId`,`variable`);

--
-- Indices de la tabla `bacata_votocodificado`
--
ALTER TABLE `bacata_votocodificado`
  ADD PRIMARY KEY (`idvoto`);

--
-- Indices de la tabla `bacata_votodecodificado`
--
ALTER TABLE `bacata_votodecodificado`
  ADD PRIMARY KEY (`idvoto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bacata_bitacora`
--
ALTER TABLE `bacata_bitacora`
  MODIFY `id_nota` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `bacata_bloque`
--
ALTER TABLE `bacata_bloque`
  MODIFY `id_bloque` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `bacata_candidato`
--
ALTER TABLE `bacata_candidato`
  MODIFY `idcandidato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `bacata_configuracion`
--
ALTER TABLE `bacata_configuracion`
  MODIFY `id_parametro` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `bacata_dependencias`
--
ALTER TABLE `bacata_dependencias`
  MODIFY `id_dependencia` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `bacata_eleccion`
--
ALTER TABLE `bacata_eleccion`
  MODIFY `ideleccion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `bacata_lista`
--
ALTER TABLE `bacata_lista`
  MODIFY `idlista` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `bacata_llave_seguridad`
--
ALTER TABLE `bacata_llave_seguridad`
  MODIFY `idllave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `bacata_logger`
--
ALTER TABLE `bacata_logger`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `bacata_nodos`
--
ALTER TABLE `bacata_nodos`
  MODIFY `id_nodo` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `bacata_pagina`
--
ALTER TABLE `bacata_pagina`
  MODIFY `id_pagina` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `bacata_preguntasegundaclave`
--
ALTER TABLE `bacata_preguntasegundaclave`
  MODIFY `idpregunta` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `bacata_procesoelectoral`
--
ALTER TABLE `bacata_procesoelectoral`
  MODIFY `idprocesoelectoral` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `bacata_subsistema`
--
ALTER TABLE `bacata_subsistema`
  MODIFY `id_subsistema` int(7) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `bacata_tipoestamento`
--
ALTER TABLE `bacata_tipoestamento`
  MODIFY `idtipo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `bacata_tiporestriccion`
--
ALTER TABLE `bacata_tiporestriccion`
  MODIFY `idtipo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `bacata_tiporesultados`
--
ALTER TABLE `bacata_tiporesultados`
  MODIFY `idtipo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `bacata_tipousuario`
--
ALTER TABLE `bacata_tipousuario`
  MODIFY `idtipo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `bacata_tipovotacion`
--
ALTER TABLE `bacata_tipovotacion`
  MODIFY `idtipo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `bacata_votocodificado`
--
ALTER TABLE `bacata_votocodificado`
  MODIFY `idvoto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `bacata_votodecodificado`
--
ALTER TABLE `bacata_votodecodificado`
  MODIFY `idvoto` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
