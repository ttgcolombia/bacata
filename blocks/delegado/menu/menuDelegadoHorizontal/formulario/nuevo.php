<?php
$esteBloque = $this->miConfigurador->getVariableConfiguracion("esteBloque");

$rutaBloque = $this->miConfigurador->getVariableConfiguracion("host");
$rutaBloque.=$this->miConfigurador->getVariableConfiguracion("site") . "/blocks/";
$rutaBloque.= $esteBloque['grupo'] . "/" . $esteBloque['nombre'];

$directorio = $this->miConfigurador->getVariableConfiguracion("host");
$directorio.= $this->miConfigurador->getVariableConfiguracion("site") . "/index.php?";
$directorio.=$this->miConfigurador->getVariableConfiguracion("enlace");
$miSesion = Sesion::singleton();

// Definimos todos los enlaces a crear

//Inicio pagina administrador
$enlaceIndiceDelegado['enlace'] = "pagina=indexDelegado";
$enlaceIndiceDelegado['enlace'].= "&usuario=" . $miSesion->getSesionUsuarioId();

$enlaceIndiceDelegado['urlCodificada'] = $this->miConfigurador->fabricaConexiones->crypto->codificar_url($enlaceIndiceDelegado['enlace'], $directorio);
$enlaceIndiceDelegado['nombre'] = "Principal";

//Procesos Electorales

//Primer item no tiene url asociada
$enlaceProcesoElectoral['nombre'] = "Procesos Electorales";

//Crear Proceso electoral
$enlaceIniciarProceso['enlace'] = "pagina=iniciarProceso";
$enlaceIniciarProceso['enlace'].= "&usuario=" . $miSesion->getSesionUsuarioId();
$enlaceIniciarProceso['enlace'].= "&opcion=paso1";
$enlaceIniciarProceso['enlace'].= "&tiempo=" . time(); 
$enlaceIniciarProceso['urlCodificada'] = $this->miConfigurador->fabricaConexiones->crypto->codificar_url($enlaceIniciarProceso['enlace'], $directorio);
$enlaceIniciarProceso['nombre'] = "Iniciar Proceso ";


//Crear Proceso electoral
$enlaceSeguirProceso['enlace'] = "pagina=seguimientoProceso";
$enlaceSeguirProceso['enlace'].= "&usuario=" . $miSesion->getSesionUsuarioId();
$enlaceSeguirProceso['urlCodificada'] = $this->miConfigurador->fabricaConexiones->crypto->codificar_url($enlaceSeguirProceso['enlace'], $directorio);
$enlaceSeguirProceso['nombre'] = "Monitorear Proceso ";

//Crear Proceso electoral
$enlaceCerrarProceso['enlace'] = "pagina=cerrarProceso";
$enlaceCerrarProceso['enlace'].= "&usuario=" . $miSesion->getSesionUsuarioId();
$enlaceCerrarProceso['urlCodificada'] = $this->miConfigurador->fabricaConexiones->crypto->codificar_url($enlaceCerrarProceso['enlace'], $directorio);
$enlaceCerrarProceso['nombre'] = "Cerrar Proceso ";

//Cambiar Clave acceso
$enlaceCambiarClave['enlace'] = "pagina=cambiarClaveDelegado";
$enlaceCambiarClave['enlace'].= "&usuario=" . $miSesion->getSesionUsuarioId();
$enlaceCambiarClave['urlCodificada'] = $this->miConfigurador->fabricaConexiones->crypto->codificar_url($enlaceCambiarClave['enlace'], $directorio);
$enlaceCambiarClave['nombre'] = "Cambiar Contraseña";

//Cerrar Sesion
$enlaceCerrarSesion['enlace'] = "pagina=cerrarSesionDelegado";
$enlaceCerrarSesion['enlace'].= "&usuario=" . $miSesion->getSesionUsuarioId();
$enlaceCerrarSesion['urlCodificada'] = $this->miConfigurador->fabricaConexiones->crypto->codificar_url($enlaceCerrarSesion['enlace'], $directorio);
$enlaceCerrarSesion['nombre'] = "Salir";

?>
<nav id="cbp-hrmenu" class="cbp-hrmenu">
    <ul>
       
        <li><a href="#">Inicio</a>
            <div class="cbp-hrsub">
                <div class="cbp-hrsub-inner">
                    <div><br>
                        <ul>
                            <li><a href="<?php echo $enlaceIndiceDelegado['urlCodificada'];?>"><?php echo $enlaceIndiceDelegado['nombre']?></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /cbp-hrsub-inner -->
            </div> <!-- /cbp-hrsub -->
        </li>
        <li><a href="#">Gestión Procesos</a>
            <div class="cbp-hrsub">
                <div class="cbp-hrsub-inner">
                    <div><br>
                        <ul>
                            <li><a href="<?php echo $enlaceIniciarProceso['urlCodificada'];?>"> <?php echo $enlaceIniciarProceso['nombre']?></a></li>
                            <li><a href="<?php echo $enlaceSeguirProceso['urlCodificada'];?>"> <?php echo $enlaceSeguirProceso['nombre']?></a></li>
                            <li><a href="<?php echo $enlaceCerrarProceso['urlCodificada'];?>"> <?php echo $enlaceCerrarProceso['nombre']?></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /cbp-hrsub-inner -->
            </div> <!-- /cbp-hrsub -->
        </li>        
        <li><a href="#">Mi Sesión</a>
            <div class="cbp-hrsub">
                <div class="cbp-hrsub-inner">
                    <div><br>
                        <!--h4>Usuario: <?php echo $datosUsuario[0]['NOMBRE'] . " " . $datosUsuario[0]['APELLIDO'] ?></h4-->
                        <ul>
                            <li><a href="<?php echo $enlaceCambiarClave['urlCodificada'];?>"><?php echo $enlaceCambiarClave['nombre']?></a></li>
                            <li><a href="<?php echo $enlaceCerrarSesion['urlCodificada'];?>"><?php echo $enlaceCerrarSesion['nombre']?></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /cbp-hrsub-inner -->
            </div> <!-- /cbp-hrsub -->
        </li>
    </ul>
</nav>
