<?php
//Votos x hora
$cadena_sql = $this->sql->cadena_sql("totalVotaciones", '');
$resultadosCertificados = $esteRecursoDB->ejecutarAcceso($cadena_sql, "busqueda");

$series1 = "";
$labels1 = "";
$totalVotos = 0;

if ($resultadosCertificados) {
    for ($i = 0; $i < count($resultadosCertificados); $i++) {
        if ($resultadosCertificados[$i][0] == date('Y m d')) {
            if (($i + 1) == (count($resultadosCertificados))) {
                $series1 .= $resultadosCertificados[$i][3];
                $labels1 .= "'" . $resultadosCertificados[$i][2] . "'";
                
            } else {
                $series1 .= $resultadosCertificados[$i][3] . ", ";
                $labels1 .= "'" . $resultadosCertificados[$i][2] . "', ";
            }
            $totalVotos+=$resultadosCertificados[$i][3];
        }
    }
} else {
    $series1 = "0,0";
    $labels1 = "0,0";
}

if($series1 == "" || $labels1=="")
    {
        $series1 = "0,0";
        $labels1 = "0,0";
    }
?>

<script type='text/javascript'>

    $(document).ready(function() {
        $.jqplot.config.enablePlugins = true;
        //var s1 = [2, 6, 7, 10];
        var s1 = [<?php echo $series1 ?>];
        var ticks1 = [<?php echo $labels1 ?>];


        plot1 = $.jqplot('votosHora', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults: {
                renderer: $.jqplot.BarRenderer,
                pointLabels: {show: true}
            },
            title: 'Votos Realizados por hora - Total Votos <?php echo $totalVotos ?>',
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks1
                }
            },
            highlighter: {show: true}
        });

    });
</script>