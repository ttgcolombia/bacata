<?php
//Accesos Fallidos
$cadena_sql = $this->sql->cadena_sql("usuarioNoExiste", '');
$resultadosUsuario = $esteRecursoDB->ejecutarAcceso($cadena_sql, "busqueda");

$series1 = "";
$labels1 = "";

if ($resultadosUsuario) {
    for ($i = 0; $i < count($resultadosUsuario); $i++) {
        if ($resultadosUsuario[$i][0] == date('Y m d')) {
            if (($i + 1) == (count($resultadosUsuario))) {
                $series1 .= $resultadosUsuario[$i][3];
                $labels1 .= "'" . $resultadosUsuario[$i][2] . "'";
            } else {
                $series1 .= $resultadosUsuario[$i][3] . ", ";
                $labels1 .= "'" . $resultadosUsuario[$i][2] . "', ";
            }
        }
    }
} else {
    $series1 = "0,0";
    $labels1 = "0,0";
}


if($series1 == "" || $labels1=="")
    {
        $series1 = "0,0";
        $labels1 = "0,0";
    }
?>

<script type='text/javascript'>

    $(document).ready(function() {
        $.jqplot.config.enablePlugins = true;
        //var s1 = [2, 6, 7, 10];
        var s1 = [<?php echo $series1 ?>];
        var ticks1 = [<?php echo $labels1 ?>];


        plot1 = $.jqplot('accesoFallido', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults: {
                renderer: $.jqplot.BarRenderer,
                pointLabels: {show: true}
            },
            title: 'Accesos fallidos a la plataforma virtual, usuario invalido',
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks1
                }
            },
            highlighter: {show: true}
        });

    });
</script>