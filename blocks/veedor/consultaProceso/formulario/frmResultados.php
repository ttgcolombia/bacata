<?php
if (!isset($GLOBALS["autorizado"])) {
    include("../index.php");
    exit;
}

/**
 * La conexiòn que se debe utilizar es la principal de SARA
 */
$conexion = "estructura";
$esteRecursoDB = $this->miConfigurador->fabricaConexiones->getRecursoDB($conexion);

$cadena_sql = $this->sql->cadena_sql('zonaHoraria', '');
$resultadosAcceso = $esteRecursoDB->ejecutarAcceso($cadena_sql, 'acceso');



echo "<center><h4>Seguimiento del proceso " . date('d M Y') . "</h4></center>";
?>
<table width="100%">

    <tr>
        <td><div id='votosHora'></div></td>
        <td><div id='votantesFaltantes'></div></td>
    </tr>
    <tr>
        <td><div id='votantesEstamento'></div></td>
        <td><div id='totalVotantes'></div></td>
    </tr>
    <tr>
        <td><div id='accesos'></div></td>
        <td><div id='accesoFallido'></div></td>
    </tr>
    <tr>
        <td><div id='claveErrada'></div></td>
        <td><div ></div></td>
    </tr>
</table>

<?php include("graficos/votosHora.php"); ?>
<?php include("graficos/totalVotantes.php"); ?>
<?php include("graficos/totalVotantesFaltantes.php"); ?>
<?php include("graficos/totalVotantesEstamento.php"); ?>

<?php //include("graficos/accesos.php"); ?>
<?php //include("graficos/accesoFallido.php"); ?>
<?php //include("graficos/claveErrada.php"); ?>
